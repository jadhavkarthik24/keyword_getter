import os
import json
import boto3
import threading
import pandas as pd
import numpy as np
from .models import *
from .serializers import *
from dotenv import load_dotenv
from django.conf import settings
from rest_framework import status
from datetime import datetime, timedelta
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
# from django.shortcuts import render, redirect
from rest_framework.renderers import TemplateHTMLRenderer
# from django.core.files.storage import FileSystemStorage
# from django.core.serializers.python import Serializer

# accessing .env file
BASE_DIR = settings.BASE_DIR
env_path = os.path.join(BASE_DIR, ".env")
load_dotenv(dotenv_path=env_path)

session = boto3.Session(
    aws_access_key_id= os.environ['ACCESS_KEY_ID'],
    aws_secret_access_key = os.environ['SECRET_ACCESS_KEY']
)

# uploads file to s3
def uploadFile(csv_file,key):
    s3 = boto3.resource(
        's3',
        region_name = os.environ['REGION'], 
        aws_access_key_id = os.environ['ACCESS_KEY_ID'],
        aws_secret_access_key = os.environ['SECRET_ACCESS_KEY']
        )
    try:
        object = s3.Object(os.environ['BUCKET_NAME'], "{}/{}".format(key,csv_file))
        if isinstance(csv_file,str):
            upload_file_path = os.path.join(BASE_DIR,"keywords_data",csv_file)
            csv_file = open(upload_file_path, 'rb')
        object.put(ACL='public-read',Body=csv_file)
        return True
    except Exception as e:
        return e

@api_view(['POST', ])
def uploadcsvapi(request):
    if request.method == 'POST' and request.FILES['csvfile']:
        csv_file = request.FILES['csvfile']
        if not csv_file.name.endswith('.csv'):
            return Response({"message": "Please upload csv file"}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        # fs = FileSystemStorage()
        # filename = fs.save(csv_file.name, csv_file)

        # uploaded_file_url = fs.url(filename)
        # filepath = settings.MEDIA_ROOT+filename
        # filename = os.path.basename(csv_file.name)
        filename = csv_file.name.split()
        region = 'us-east-1'
        bucket_name = 'keywords-data'
        key = 'csvfiles/{}'.format(filename.replace(" ","%20"))
        csv_dict = {
            'csv_filename': filename.split(".")[0],
            # 'file_path': key,
            'file_url' :  "https://{}.s3.amazonaws.com/{}".format(bucket_name, key)
        }
        serializer = CsvTrackerSerializer(data=csv_dict)
        if serializer.is_valid():
            serializer.save()
        else:
            print(serializer.errors)
        return Response({"message": "Successfully uploaded"}, status=status.HTTP_200_OK)
    return Response({"message": "Invalid method"}, status=status.HTTP_405_METHOD_NOT_ALLOWED)

@api_view(['GET', ])
def gets3cred(request):
    # BASE_DIR = settings.BASE_DIR
    # env_path = os.path.join(BASE_DIR, ".env")
    # load_dotenv(dotenv_path=env_path)
    # # .evn variables
    # response = {
    #     "REGION" : os.environ['REGION'], 
    #     "ACCESS_KEY_ID" : os.environ['ACCESS_KEY_ID'], 
    #     "SECRET_ACCESS_KEY" : os.environ['SECRET_ACCESS_KEY'],
    #     "BUCKET_NAME" : os.environ['BUCKET_NAME'],
    #     "KEY" : os.environ['KEY'],
    # }

    response = {
        "REGION" : 'us-east-1', 
        "ACCESS_KEY_ID" : 'AKIA6H7DLV47AXXG2EZV', 
        "SECRET_ACCESS_KEY" : '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',
        "BUCKET_NAME" : 'keywords-data',
        "KEY" : 'csvfiles/',
    }
    return Response(response, status=status.HTTP_200_OK)

def bgmClusteringAlgo(keywords,ofile,categories):
    # universal-sentencing-encoder model libraries
    # import tensorflow as tf
    # import tensorflow_hub as hub
    import pickle
    
    # use_model_path = os.path.join(BASE_DIR, "use_4")
    # USE_model = hub.load(use_model_path)
    # # fetches embedding of keywords
    # def embed(input):
    #     return USE_model(input)
    # embeddings = embed(keywords)
    # print(embeddings)
    embedding_file_path = os.path.join(BASE_DIR,"USE_emb.npy")
    emb = np.load(embedding_file_path)
    keywords_count = len(keywords)
    # print(keywords_count)
    embeddings = emb[:keywords_count]
    bgm_model_path = os.path.join(BASE_DIR,"clustering.pickle")
    with open(bgm_model_path, 'rb') as f:
        bgm = pickle.load(f) 
    labels_bgm = bgm.fit_predict(embeddings)
    # print(labels_bgm)
    clustered_data = pd.DataFrame(columns=['clustered_id','keyword',"category"])
    clustered_data['clustered_id'] = labels_bgm
    clustered_data['keyword'] = keywords
    clustered_data['category'] = categories
    category = ofile
    ofile = ofile.replace(" ","+")+"_clustered_data.csv"
    upload_file_path = os.path.join(BASE_DIR,"keywords_data",ofile)
    clustered_data.to_csv(upload_file_path,encoding='utf-8', index=False)

    key = "clustered-data"
    uploaded_status = uploadFile(ofile, key)
    s3_folder = "{}/{}".format(key,ofile)
    if uploaded_status == True:
        data = {
            "category":category,
            "file_url": "https://{}.s3.amazonaws.com/{}".format(os.environ['BUCKET_NAME'], s3_folder.replace("+", "%2B")),
            "s3_folder_name":key
        }
        serializer = S3FilesSerializer(data= data)
        if serializer.is_valid():
            serializer.save()
        else:
            print(serializer.errors)
    return True
    
class SeedKeywordUpdate(APIView):
    """
    Retrieve, update or delete a SeedKeywords instance.
    """

    def get_object(self, pk):
        try:
            return SeedKeywords.objects.get(pk=pk)
        except SeedKeywords.DoesNotExist:
            raise Http404

    def put(self, request, pk, format=None):
        seed_keyword = self.get_object(pk)
        serializer = UpdateSerializer(seed_keyword, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SeedKeywordsList(APIView):
    """
    List all seed keywords.
    """
    def get(self, request, format=None):
        fetching_status = request.GET["keyword_fetching_status"]
        username = request.GET["username"]
        seed_keywords = SeedKeywords.objects.filter(
            keyword_fetching_status=0).order_by('id')[:1]
        if fetching_status == "1" and username!="":
            seed_keywords = SeedKeywords.objects.filter(keyword_fetching_status=1,user_fetched=username).order_by('id')[:1]
        serializer = SeedKeywordsSerializer(seed_keywords, many=True)
        return Response(serializer.data)

class CsvFilesList(APIView):
    """ List all csv files in the s3 """
    def get(self, request, format=None):
        csvfiles = CSVFilesTracker.objects.filter(file_uploaded_status=False).order_by('id')
        serializer = CsvTrackerSerializer(csvfiles, many=True)
        return Response(serializer.data)

@api_view(['POST', ])
def uploadfiletos3(request):
    if request.method == 'POST' and request.FILES['csvfile']:
        params = request.POST
        csv_file = request.FILES['csvfile']
        if not csv_file.name.endswith('.csv'):
            return Response({"message": "Please upload csv file"}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        filename = csv_file.name
        category = filename.split(".")[0]
        index = category.find("_keywords")
        if index != -1:
            category = category[:index] 
        if 'key' not in params.keys():
            return Response({"message": "Missing key parameter"}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        key = params['key']
        uploaded_status = uploadFile(csv_file, key)
        s3_folder = "{}/{}".format(key,filename.replace(" ","%20"))
        if uploaded_status == True:
            data = {
                "category":category,
                "file_url": "https://{}.s3.amazonaws.com/{}".format(os.environ['BUCKET_NAME'], s3_folder),
                "s3_folder_name":key
            }
            serializer = S3FilesSerializer(data= data)
            if serializer.is_valid():
                serializer.save()
        return Response({"message": "Successfully uploaded"}, status=status.HTTP_200_OK)
    return Response({"message": "Invalid method"}, status=status.HTTP_405_METHOD_NOT_ALLOWED)

class AddIssue(APIView):
    """ Inserts issues to Issue log table """
    def post(self, request, format=None):
        serializer = IssuesLogSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class IssuesLogView(APIView):
    """ Lists all the Issues """
    def get(self, request, format=None):
        issues = IssuesLog.objects.all()
        serializer = IssuesLogSerializer(issues,many=True)
        return Response(serializer.data)

class HomeView(APIView):
    """ List all csv files in the s3 """
    renderer_classes = [TemplateHTMLRenderer]

    def get(self, request, format=None):
        csvfiles = CSVFilesTracker.objects.filter(file_uploaded_status=False,s3_folder_name="csvfiles").order_by('id')
        serializer = CsvTrackerSerializer(csvfiles, many=True)
        return Response({"data" : serializer.data},template_name='base.html')

class Process2_4DataView(APIView):
    renderer_classes = [TemplateHTMLRenderer]

    def get(self, request, format=None):
        params = request.GET
        csvfiles = CSVFilesTracker.objects.filter(file_uploaded_status=False,s3_folder_name="csvfiles").order_by('id')
        if 'key' in params.keys():
            key = params['key']
            csvfiles = CSVFilesTracker.objects.filter(file_uploaded_status=False,s3_folder_name=key).order_by('id')
        serializer = CsvTrackerSerializer(csvfiles, many=True)
        return Response({"data" : serializer.data},template_name='process2_4.html')

@api_view(['POST', ])
def getclustereddata(request):
    """ standardize input and output  
    input format:: 
    [
        {
            "category" : airpod,
            "keyword" : "airpods pro",
        },
        {
            "category" : airpod,
            "keyword" : "airpods apple",
        }
    ]
    output format :: 
    [
        {
            "category" : airpod,
            "keyword" : "airpods pro",
            "cluster_id":1
        },
        {
            "category" : airpod,
            "keyword" : "airpods apple",
            "cluster_id":2
        }
    ]
    """
    if request.method == 'POST' and (request.FILES.get('ifile', False) or request.POST.get('s3_file', False)):
        params = request.POST
        headers_ = ["meta_keyword","keyword"]
        category = ""
        if "s3_file" in params.keys():
            file_url = params['s3_file']
            category = params['category']
            df = pd.read_csv(file_url)
        if "ifile" in request.FILES:
            file_extension = [".csv",".json"]
            ifile = request.FILES['ifile']
            filename = ifile.name
            category = filename.split(".")[0]
            index = category.find("_keywords")
            if index != -1:
                category = category[:index] 
            # category = category
            if not ifile.name.endswith(tuple(file_extension)):
                return Response({"message": "Please upload csv or json file"}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
            keywords = []
            if ifile.name.endswith(".json"):
                data = json.load(ifile)
                df = pd.DataFrame(data)
                # df =  pd.json_normalize(data, record_path =['keywords'],meta=['category'])
                # keywords = df[0].tolist()
            else:
                df = pd.read_csv(ifile)
            
        columns = df.columns
        columns_intersections = list(set(columns) & set(headers_))
        if len(columns_intersections) != len(headers_):
            return Response({"message": "Missing headers category or keyword"}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        t1=threading.Thread(target=bgmClusteringAlgo,args=(df['keyword'],category,df['meta_keyword']))
        t1.start()
        return Response({"message" : "success"}, status=status.HTTP_200_OK)
    else:
        return Response({"message": "Missing post parmaters"}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)