import os
import datetime
from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.core.validators import MaxValueValidator, MinValueValidator
import pandas as pd
from django.conf import settings
from .search_volume import *
# Create your models here.

BASE_DIR = settings.BASE_DIR

def current_year():
    return datetime.date.today().year

class SeedKeywords(models.Model):
    KEYWORD_CHOICES = (
        (0, 'Not Started'),
        (1, 'Inprocess'),
        (2, 'Completed'),
    )
    keyword = models.CharField(max_length=255)
    search_volume = models.IntegerField(null=True)
    cpc = models.FloatField(null=True, blank=True)
    industry = models.CharField(max_length=100, null=True)
    competition = models.FloatField(null=True, blank=True)
    # status checks whether related keywords fetched or not for a seed keyword
    keyword_fetching_status = models.SmallIntegerField(
        null=True, choices=KEYWORD_CHOICES, default=0)
    added_dt = models.DateField(auto_now_add=True, blank=True, null=True)
    user_fetched = models.CharField(max_length=50,blank=True, null=True)
    status_changed_dt = models.DateTimeField(null=True)

    def __str__(self):
        return self.keyword

    class Meta:
        ordering = ('-search_volume',)
        indexes = [
            models.Index(fields=['keyword', ]),
        ]


class Keywords(models.Model):
    # seed keyword
    keyword = models.TextField(max_length=512)
    search_volume = models.IntegerField(null=True)
    seed_keyword = models.CharField(max_length=255, null=True)
    # parent keywords through which all the keywords are fetched
    meta_keyword = models.CharField(max_length=255, null=True)
    relevancy_score = models.IntegerField(null=True)
    low_top_of_page_bid = models.FloatField(default=0)
    high_top_of_page_bid = models.FloatField(default=0)
    competition = models.CharField(max_length=12,default="UNKNOWN")
    competition_index = models.PositiveIntegerField(default=0,validators=[
            MaxValueValidator(100),
            MinValueValidator(0)
        ])
    added_dt = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    is_question = models.BooleanField(default=False)

    def __str__(self):
        return self.keyword

    class Meta:
        ordering = ('-search_volume',)
        indexes = [
            models.Index(fields=['keyword', 'meta_keyword', ]),
        ]

class MonthlySearchVolume(models.Model):
    keyword = models.ForeignKey(Keywords, on_delete=models.CASCADE)
    year = models.PositiveIntegerField(default=current_year())
    month = models.CharField(max_length=20)
    monthly_searches = models.IntegerField()
    created_dt = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return self.keyword.keyword


class CSVFilesTracker(models.Model):
    category = models.CharField(max_length=255, null=True)
    file_path = models.CharField(max_length=255, blank=True,null=True)
    file_url = models.URLField(max_length = 255,blank=True,null=True)
    file_uploaded_status = models.BooleanField(default=False)
    s3_folder_name = models.CharField(max_length=50, blank=True,null=True)
    short_url = models.URLField(max_length = 255,blank=True,null=True)

    def __str__(self):
        return self.category

    class Meta:
        indexes = [
            models.Index(fields=['file_uploaded_status', ]),
        ]


class CronTracker(models.Model):
    cron_name = models.CharField(max_length=100, null=True)
    cron_running_status = models.BooleanField(default=False)

    def __str__(self):
        return self.cron_name

class IssuesLog(models.Model):
    user = models.CharField(max_length=50, null=True)
    seed_keyword = models.CharField(max_length=255, null=True)
    meta_keyword = models.CharField(max_length=100, null=True)
    log_info = models.TextField(null=True)
    log_time = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return self.log_info


# @receiver(post_save, sender=CSVFilesTracker)
# def update_search_volume(sender, instance, created, **kwargs):
#     if created:
#         url = instance.file_url
#         df = pd.read_csv(url)
#         cluster_centroid = df['cluster_centroid'].str.strip().replace('[^a-zA-Z0-9 ]+','',regex=True)
#         target_keywords = cluster_centroid.tolist()
#         print(type(target_keywords))
#         # googleads client 
#         google_client_file_path = os.path.join(BASE_DIR, "google-ads-client.yaml")
#         googleads_client = GoogleAdsClient.load_from_storage(google_client_file_path,version="v7")
#         results = main(client, "4731829043", target_keywords)
#         volume_data = pd.json_normalize(results, meta=['keyword','avg_monthly_searches','competition','competition_index','low_top_of_page_bid','high_top_of_page_bid'],record_path =['monthly_search_volumes'])
#         volume_data.to_csv("airpods_process3.csv",encoding='utf-8', index=False)

