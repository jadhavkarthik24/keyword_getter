import os
import csv
from google.ads.googleads.client import GoogleAdsClient
from google.ads.googleads.errors import GoogleAdsException
from .serializers import KeywordsSerializer, CronSerializer , AddMonthlySearchVolumeSerializer
from .models import CSVFilesTracker, CronTracker,Keywords
from .search_volume import *


def fetchCronStatus():
    crons = CronTracker.objects.all()
    if len(crons) == 0:
        serializer_data = {
            "cron_name": "uploadKeywordsData",
        }
        serializer = CronSerializer(data=serializer_data)
        if serializer.is_valid():
            serializer.save()
        crons = CronTracker.objects.all()
        obj = crons[0]
        obj.cron_running_status = True
        obj.save()
        return True
    else:
        if crons[0].cron_running_status == False:
            obj = crons[0]
            obj.cron_running_status = True
            obj.save()
            return True
        else:
            return False


def uploadKeywordsData():
    if fetchCronStatus():
        files = CSVFilesTracker.objects.filter(file_uploaded_status=False)
        if len(files) > 0:
            files = files[:1]
            file = files[0]
            ifile = open(file.file_path, 'r')
            reader = csv.DictReader(ifile)
            for row in reader:
                serializer = KeywordsSerializer(data=row)
                if serializer.is_valid():
                    serializer.save()
            file.file_uploaded_status = True
            file.save()
        crons = CronTracker.objects.all()
        obj = crons[0]
        obj.cron_running_status = False
        obj.save()
        return
    else:
        return

def updateSearchVolume():
    keywords = Keywords.objects.filter(search_volume__isnull=True).values_list('keyword',flat=True).order_by('id')[:100]
    if len(keywords) > 0:
        target_keywords = list(keywords)
        googleads_client = GoogleAdsClient.load_from_storage(version="v7")
        try:
            results = main(googleads_client, "4731829043",target_keywords)
            for result in results:
                print(result)
                try:
                # updates search volume and other details for a key
                    obj = Keywords.objects.get(keyword=result['keyword'])
                    obj.search_volume = result['avg_monthly_searches']
                    obj.competition = result['competition']
                    obj.competition_index = result['competition_index']
                    obj.low_top_of_page_bid = result['low_top_of_page_bid']
                    obj.high_top_of_page_bid = result['high_top_of_page_bid']
                    obj.save()
                    # adds monthly search volume
                    monthly_search_volumes = result['monthly_search_volumes']
                    for monthly_search_volume in monthly_search_volumes:
                        monthly_search_volume["keyword"] = obj.id
                    if len(monthly_search_volumes) > 0 :
                        serializer = AddMonthlySearchVolumeSerializer(data=monthly_search_volumes,many=True)
                        if serializer.is_valid():
                            serializer.save()
                except:
                    continue

        except GoogleAdsException as ex:
            print(
                f'Request with ID "{ex.request_id}" failed with status '
                f'"{ex.error.code().name}" and includes the following errors:'
            )
            for error in ex.failure.errors:
                print(f'    Error with message "{error.message}".')
                if error.location:
                    for field_path_element in error.location.field_path_elements:
                        print(f"\t\tOn field: {field_path_element.field_name}")
            sys.exit(1)
    return
