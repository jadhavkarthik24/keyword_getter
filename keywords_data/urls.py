from django.urls import path, include
from .views import *

urlpatterns = [
    # path('upload/', uploadcsv, name='upload-csv'),
    path('s3/credentials/', gets3cred, name='get-s3-credentials'),
    path('seedkeywords/list/', SeedKeywordsList.as_view(),
         name='seedkeywords-list'),
    path('csvfiles/', CsvFilesList.as_view(),
         name='s3-csvfiles-list'),
    path('update/<int:pk>/', SeedKeywordUpdate.as_view(), name='seedkeywords-status-update'),
    path('upload/csvfile/', uploadcsvapi, name='upload-csv'),
    path('s3/upload/',uploadfiletos3),
    path('add/issue/', AddIssue.as_view(),
         name='add-issue'),
    path('issues/list/', IssuesLogView.as_view(),
         name='issues-list'), 
    path('', HomeView.as_view(),
         name='home'),
    path('files/', Process2_4DataView.as_view(),
         name='displays-files-without-duplicates'),    
    path('cluster/data/',getclustereddata),
]
