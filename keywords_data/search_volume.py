import argparse
import sys
import csv
import pandas as pd
import json
import uuid
from google.ads.googleads.client import GoogleAdsClient
from google.ads.googleads.errors import GoogleAdsException

ONE_MILLION = 1.0e6

def main(client, customer_id,target_keywords):
    """Adds a keyword plan, campaign, ad group, etc. to the customer account.

    Also handles errors from the API and prints them.

    Args:
        client: An initialized instance of GoogleAdsClient
        customer_id: A str of the customer_id to use in requests.
    """
    try:
        resource_name = _add_keyword_plan(client, customer_id, target_keywords)
    except GoogleAdsException as ex:
        print(f'Request with ID "{ex.request_id}" failed with status '
              f'"{ex.error.code().name}" and includes the following errors:')
        for error in ex.failure.errors:
            print(f'\tError with message "{error.message}".')
            if error.location:
                for field_path_element in error.location.field_path_elements:
                    print(f'\t\tOn field: {field_path_element.field_name}')
        sys.exit(1)
    # keywordplan service
    keyword_plan_service = client.get_service("KeywordPlanService")
    # fetches competition enumerate
    keyword_competition_level_enum = client.get_type('KeywordPlanCompetitionLevelEnum').KeywordPlanCompetitionLevel
    # calls generate historical metrics service
    try:
        response = keyword_plan_service.generate_historical_metrics(
            keyword_plan=resource_name
        )
    except GoogleAdsException as ex:
        print('Request with ID "{}" failed with status "%s" and includes the '
              'following errors:'.format(ex.request_id, ex.error.code().name))
        for error in ex.failure.errors:
            print('\tError with message "{}".'.format(error.message))
            if error.location:
                for field_path_element in error.location.field_path_elements:
                    print('\t\tOn field: {}'.format(field_path_element.field_name))
        sys.exit(1)
    # keywords metrics
    results = []
    for i, historical in enumerate(response.metrics):
        metrics = historical.keyword_metrics
        results.append({
            'keyword': historical.search_query,
            'avg_monthly_searches': metrics.avg_monthly_searches,
            'competition': keyword_competition_level_enum(metrics.competition).name,
            'competition_index': metrics.competition_index,
            'low_top_of_page_bid':  metrics.low_top_of_page_bid_micros / ONE_MILLION,
            'high_top_of_page_bid': metrics.high_top_of_page_bid_micros / ONE_MILLION,
            "monthly_search_volumes": [{"year":monthly_volume.year,"month":monthly_volume.month.name,"monthly_searches":monthly_volume.monthly_searches} for monthly_volume in metrics.monthly_search_volumes]
        })
    
    return results
    # with open("keyword_historical_metrics.json", 'w', encoding='utf-8') as f:
    #     json.dump(results, f, ensure_ascii=False, indent=4)


def _add_keyword_plan(client, customer_id,target_keywords):
    """Adds a keyword plan, campaign, ad group, etc. to the customer account.

    Args:
        client: An initialized instance of GoogleAdsClient
        customer_id: A str of the customer_id to use in requests.

    Raises:
        GoogleAdsException: If an error is returned from the API.
    """
    keyword_plan = _create_keyword_plan(client, customer_id)
    keyword_plan_campaign = _create_keyword_plan_campaign(
        client, customer_id, keyword_plan
    )
    keyword_plan_ad_group = _create_keyword_plan_ad_group(
        client, customer_id, keyword_plan_campaign
    )
    _create_keyword_plan_ad_group_keywords(
        client, customer_id, keyword_plan_ad_group,target_keywords
    )
    # _create_keyword_plan_negative_campaign_keywords(
    #     client, customer_id, keyword_plan_campaign
    # )
    return keyword_plan


def _create_keyword_plan(client, customer_id):
    """Adds a keyword plan to the given customer account.

    Args:
        client: An initialized instance of GoogleAdsClient
        customer_id: A str of the customer_id to use in requests.

    Returns:
        A str of the resource_name for the newly created keyword plan.

    Raises:
        GoogleAdsException: If an error is returned from the API.
    """
    keyword_plan_service = client.get_service("KeywordPlanService")
    operation = client.get_type("KeywordPlanOperation")
    keyword_plan = operation.create

    keyword_plan.name = f"Keyword plan for traffic estimate {uuid.uuid4()}"

    forecast_interval = client.get_type(
        "KeywordPlanForecastIntervalEnum"
    ).KeywordPlanForecastInterval.NEXT_QUARTER
    keyword_plan.forecast_period.date_interval = forecast_interval

    response = keyword_plan_service.mutate_keyword_plans(
        customer_id=customer_id, operations=[operation]
    )
    resource_name = response.results[0].resource_name

    # print(f"Created keyword plan with resource name: {resource_name}")

    return resource_name


def _create_keyword_plan_campaign(client, customer_id, keyword_plan):
    """Adds a keyword plan campaign to the given keyword plan.

    Args:
        client: An initialized instance of GoogleAdsClient
        customer_id: A str of the customer_id to use in requests.
        keyword_plan: A str of the keyword plan resource_name this keyword plan
            campaign should be attributed to.create_keyword_plan.

    Returns:
        A str of the resource_name for the newly created keyword plan campaign.

    Raises:
        GoogleAdsException: If an error is returned from the API.
    """
    keyword_plan_campaign_service = client.get_service(
        "KeywordPlanCampaignService"
    )
    operation = client.get_type("KeywordPlanCampaignOperation")
    keyword_plan_campaign = operation.create

    keyword_plan_campaign.name = f"Keyword plan campaign {uuid.uuid4()}"
    keyword_plan_campaign.cpc_bid_micros = 1000000
    keyword_plan_campaign.keyword_plan = keyword_plan

    network = client.get_type(
        "KeywordPlanNetworkEnum"
    ).KeywordPlanNetwork.GOOGLE_SEARCH
    keyword_plan_campaign.keyword_plan_network = network

    geo_target = client.get_type("KeywordPlanGeoTarget")
    # Constant for U.S. Other geo target constants can be referenced here:
    # https://developers.google.com/google-ads/api/reference/data/geotargets
    geo_target.geo_target_constant = "geoTargetConstants/2840"
    keyword_plan_campaign.geo_targets.append(geo_target)

    # Constant for English
    language = "languageConstants/1000"
    keyword_plan_campaign.language_constants.append(language)

    response = keyword_plan_campaign_service.mutate_keyword_plan_campaigns(
        customer_id=customer_id, operations=[operation]
    )

    resource_name = response.results[0].resource_name

    # print(f"Created keyword plan campaign with resource name: {resource_name}")

    return resource_name


def _create_keyword_plan_ad_group(client, customer_id, keyword_plan_campaign):
    """Adds a keyword plan ad group to the given keyword plan campaign.

    Args:
        client: An initialized instance of GoogleAdsClient
        customer_id: A str of the customer_id to use in requests.
        keyword_plan_campaign: A str of the keyword plan campaign resource_name
            this keyword plan ad group should be attributed to.

    Returns:
        A str of the resource_name for the newly created keyword plan ad group.

    Raises:
        GoogleAdsException: If an error is returned from the API.
    """
    operation = client.get_type("KeywordPlanAdGroupOperation")
    keyword_plan_ad_group = operation.create

    keyword_plan_ad_group.name = f"Keyword plan ad group {uuid.uuid4()}"
    keyword_plan_ad_group.cpc_bid_micros = 2500000
    keyword_plan_ad_group.keyword_plan_campaign = keyword_plan_campaign

    keyword_plan_ad_group_service = client.get_service(
        "KeywordPlanAdGroupService"
    )
    response = keyword_plan_ad_group_service.mutate_keyword_plan_ad_groups(
        customer_id=customer_id, operations=[operation]
    )

    resource_name = response.results[0].resource_name

    # print(f"Created keyword plan ad group with resource name: {resource_name}")

    return resource_name


def _create_keyword_plan_ad_group_keywords(client, customer_id, plan_ad_group,target_keywords):
    """Adds keyword plan ad group keywords to the given keyword plan ad group.

    Args:
        client: An initialized instance of GoogleAdsClient
        customer_id: A str of the customer_id to use in requests.
        plan_ad_group: A str of the keyword plan ad group resource_name
            these keyword plan keywords should be attributed to.

    Raises:
        GoogleAdsException: If an error is returned from the API.
    """
    keyword_plan_ad_group_keyword_service = client.get_service(
        "KeywordPlanAdGroupKeywordService"
    )
    operation = client.get_type("KeywordPlanAdGroupKeywordOperation")
    operations = []
    for target_keyword in target_keywords:
        operation = client.get_type("KeywordPlanAdGroupKeywordOperation")
        keyword_plan_ad_group_keyword = operation.create
        keyword_plan_ad_group_keyword.text = target_keyword
        keyword_plan_ad_group_keyword.cpc_bid_micros = 2000000
        keyword_plan_ad_group_keyword.match_type = client.get_type(
            "KeywordMatchTypeEnum"
        ).KeywordMatchType.BROAD
        keyword_plan_ad_group_keyword.keyword_plan_ad_group = plan_ad_group
        operations.append(operation)

    response = keyword_plan_ad_group_keyword_service.mutate_keyword_plan_ad_group_keywords(
        customer_id=customer_id, operations=operations
    )
    # for result in response.results:
    #     print(
    #         "Created keyword plan ad group keyword with resource name: "
    #         f"{result.resource_name}"
    #     )

    return response
    

def _create_keyword_plan_negative_campaign_keywords(
    client, customer_id, plan_campaign
):
    """Adds a keyword plan negative campaign keyword to the given campaign.

    Args:
        client: An initialized instance of GoogleAdsClient
        customer_id: A str of the customer_id to use in requests.
        plan_campaign: A str of the keyword plan campaign resource_name
            this keyword plan negative keyword should be attributed to.

    Raises:
        GoogleAdsException: If an error is returned from the API.
    """
    keyword_plan_negative_keyword_service = client.get_service(
        "KeywordPlanCampaignKeywordService"
    )
    operation = client.get_type("KeywordPlanCampaignKeywordOperation")

    keyword_plan_campaign_keyword = operation.create
    keyword_plan_campaign_keyword.text = "moon walk"
    keyword_plan_campaign_keyword.match_type = client.get_type(
        "KeywordMatchTypeEnum"
    ).KeywordMatchType.BROAD
    keyword_plan_campaign_keyword.keyword_plan_campaign = plan_campaign
    keyword_plan_campaign_keyword.negative = True

    response = keyword_plan_negative_keyword_service.mutate_keyword_plan_campaign_keywords(
        customer_id=customer_id, operations=[operation]
    )

    print(
        "Created keyword plan campaign keyword with resource name: "
        f"{response.results[0].resource_name}"
    )


if __name__ == "__main__":
    # GoogleAdsClient will read the google-ads.yaml configuration file in the
    # home directory if none is specified.
    googleads_client = GoogleAdsClient.load_from_storage("google-ads-client.yaml",version="v7")

    parser = argparse.ArgumentParser(
        description="Creates a keyword plan for specified customer."
    )
    # The following argument(s) should be provided to run the example.
    parser.add_argument(
        "-c",
        "--customer_id",
        type=str,
        required=True,
        help="The Google Ads customer ID.",
    )
    args = parser.parse_args()
    target_keywords = ['airpod', 'airfryer', 'kindle']
    # target_keywords = ['airpod']
    try:
        df = pd.read_csv('Seed Keywords.csv')
        target_keywords = list(df.Keywords.unique())
        results = main(googleads_client, args.customer_id,target_keywords)

        for result in results:
            monthly_search_volumes = result['monthly_search_volumes']
            result.pop("monthly_search_volumes")
            for monthly_searches in monthly_search_volumes:
                ofile = open("keyword_search_volume.csv", 'a',encoding="utf-8", newline="")
                writer = csv.DictWriter(ofile, fieldnames=["keyword", "avg_monthly_searches", "competition", "competition_index","low_top_of_page_bid","high_top_of_page_bid","year","month","monthly_searches"])
                result.update(monthly_searches)
                writer.writerow(result)
                ofile.close()


    except GoogleAdsException as ex:
        print(
            f'Request with ID "{ex.request_id}" failed with status '
            f'"{ex.error.code().name}" and includes the following errors:'
        )
        for error in ex.failure.errors:
            print(f'    Error with message "{error.message}".')
            if error.location:
                for field_path_element in error.location.field_path_elements:
                    print(f"\t\tOn field: {field_path_element.field_name}")
        sys.exit(1)