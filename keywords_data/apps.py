from django.apps import AppConfig


class KeywordsDataConfig(AppConfig):
    name = 'keywords_data'
