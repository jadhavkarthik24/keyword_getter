from rest_framework import serializers
from .models import Keywords, CSVFilesTracker, SeedKeywords, CronTracker ,MonthlySearchVolume,IssuesLog
from datetime import datetime


class SeedKeywordsSerializer(serializers.ModelSerializer):
    class Meta:
        model = SeedKeywords
        fields = ('id', 'keyword', 'keyword_fetching_status',)


class UpdateSerializer(serializers.Serializer):
    keyword_fetching_status = serializers.IntegerField()
    user_fetched = serializers.CharField()
    status_changed_dt = serializers.DateTimeField(default=datetime.now())

    def update(self, instance, validated_data):
        """
        Update and return an existing `Seed keyword` instance, given the validated data.
        """
        instance.keyword_fetching_status = validated_data.get(
            'keyword_fetching_status', instance.keyword_fetching_status)
        instance.user_fetched = validated_data.get(
            'user_fetched', instance.user_fetched)
        instance.status_changed_dt = validated_data.get(
            'status_changed_dt', instance.status_changed_dt)
        instance.save()
        return instance


class KeywordsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Keywords
        fields = ('keyword', 'seed_keyword',
                  'relevancy_score', 'meta_keyword',)


class CsvTrackerSerializer(serializers.ModelSerializer):
    class Meta:
        model = CSVFilesTracker
        fields = ('category', 's3_folder_name',"file_url",)


class CronSerializer(serializers.ModelSerializer):
    class Meta:
        model = CronTracker
        fields = ('cron_name',)

class UpdateSearchVolumeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CronTracker
        fields = ('cron_name',)

class AddMonthlySearchVolumeSerializer(serializers.ModelSerializer):

    class Meta:
        model = MonthlySearchVolume
        fields = ('keyword', 'year', 'month', 'monthly_searches')

    def to_representation(self, instance):
        self.fields['keyword'] = KeywordsSerializer(read_only=True)
        return super(AddMonthlySearchVolumeSerializer, self).to_representation(instance)

class S3FilesSerializer(serializers.ModelSerializer):
    class Meta:
        model = CSVFilesTracker
        fields = ('category', "file_url","s3_folder_name",)

class IssuesLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = IssuesLog
        fields = ('user', "seed_keyword","meta_keyword","log_info","log_time",)
